package entite;

import java.util.HashMap;
import java.util.Map;

public class Photo {

	private String nom;
	private String url;
	private Map<Personne, Integer> notes;
	
	public Photo(String nom, String url, Map<Personne, Integer> notes) {
		super();
		this.nom = nom;
		this.url = url;
		this.notes = notes;
	}
	
	public Photo(String nom, String url) {
		super();
		this.nom = nom;
		this.url = url;
		this.notes = new HashMap<Personne, Integer>();
	}
	
	public void ajouterNote(Personne p, Integer note) {
		this.notes.put(p, note);
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Map<Personne, Integer> getNotes() {
		return notes;
	}
	public void setNotes(Map<Personne, Integer> notes) {
		this.notes = notes;
	}
	
}
