package application;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;



import entite.Photo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import service.Modele;

public class Controller implements Initializable {

	@FXML private ListView<String> selectImage;
	@FXML private ImageView affichage;
	@FXML private TextField inputNom;
	@FXML private PasswordField inputLogin;
	@FXML private Button boutonPersonne;
	@FXML private TextField inputNote;
	@FXML private Button boutonNote;
	@FXML private Pane appli;
	@FXML private Label erreurConversion;
	@FXML private Label erreurConnexion;
	
	Modele model = new Modele();
	ArrayList<Photo> images = model.getImages();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.appli.setVisible(false);
		ObservableList<String> liste = FXCollections.observableArrayList();;
		for (Photo img : images) {
			liste.add(img.getNom());
		}
		this.selectImage.setItems(liste);
		this.selectImage.getSelectionModel().select(0);;
		Image imgSelected = new Image(images.get(0).getUrl());
		this.affichage.setImage(imgSelected);
	}
	
	public void changerImage(MouseEvent a) {
		String nomImage = this.selectImage.getSelectionModel().getSelectedItem();
		for (Photo image : this.images) {
			if (image.getNom().equals(nomImage)) {
				this.affichage.setImage(new Image(image.getUrl()));
			}
		}
	}
	
	public void seConnecter(ActionEvent a) {
		if (this.model.checkUtilisateur(this.inputNom.getText(), this.inputLogin.getText())) {
			this.appli.setVisible(true);
			this.erreurConnexion.setText("");
		} else {
			this.appli.setVisible(false);
			this.erreurConnexion.setText("Vos identifiants sont incorrects.");
		}
	}
	
	public void noter(ActionEvent a) {

		try {
			this.model.ajouterNote(this.selectImage.getSelectionModel().getSelectedItem(), this.inputNom.getText(), Integer.parseInt(this.inputNote.getText()));
		} catch (NumberFormatException e) {
			this.erreurConversion.setText("Veuillez saisir un nombre entre 0 et 20");
	
		}
	}

	

}
