package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import entite.Photo;
import entite.Personne;

public class Modele {
	
	private ArrayList<Photo> images;
	private ArrayList<Personne> utilisateurs;

	
	public Modele() {
		super();
		
		this.images = new ArrayList<Photo>();
		this.utilisateurs = new ArrayList<Personne>();
		
		Personne p1 = new Personne("Elise", "eraffin");
		Personne p2 = new Personne("Lison", "lirissou");
		this.ajouterUtilisateur(p1);
		this.ajouterUtilisateur(p2);
		
		Photo img1 = new Photo("barcelone", "/img/barcelone.jpg");
		img1.ajouterNote(p1, 15);
		img1.ajouterNote(p2,  18);
		
		Photo img2 = new Photo("paris", "/img/paris.jpg");
		img2.ajouterNote(p1,  12);
		img2.ajouterNote(p2, 13);
		
		Photo img3 = new Photo("montreal", "/img/montreal.jpg");
		img3.ajouterNote(p1,  12);
		img3.ajouterNote(p2, 13);
		
		Photo img4 = new Photo("trondheim", "/img/trondheim.jpg");
		img4.ajouterNote(p1,  12);
		img4.ajouterNote(p2, 13);
		
		this.ajouterImage(img1);
		this.ajouterImage(img2);
		this.ajouterImage(img3);
		this.ajouterImage(img4);
	}

	
	public ArrayList<Personne> getUtilisateurs() {
		return utilisateurs;
	}


	public void ajouterUtilisateur(Personne user) {
		this.utilisateurs.add(user);
	}


	public ArrayList<Photo> getImages() {
		return images;
	}

	public void ajouterImage(Photo img) {
		this.images.add(img);
	}
	
	public Boolean checkUtilisateur(String nom, String mdp) {
		boolean flag = false;
		for (Personne p : this.utilisateurs) {
			if (p.getNom().equals(nom) && p.getLogin().equals(mdp)) {
				flag = true;
			}
		}
		return flag;
	}
	
	public void ajouterNote(String img, String user, int note) {

		
		try {

			Map<String, Map<String, Integer>> donnees = new HashMap<>();

			
			File fileNotes = new File("notes.txt");

			if (!fileNotes.exists()) {
				fileNotes.createNewFile();
			}

			FileInputStream fileTest = new FileInputStream(fileNotes);
			
			if (fileTest.available() != 0) {
				ObjectInputStream flux = new ObjectInputStream(fileTest);
				donnees = (Map<String, Map<String, Integer>>) flux.readObject();
				Map notePersonne = donnees.get(img);
				if (notePersonne == null) {
					notePersonne = new HashMap<String, Integer>();
				}
				notePersonne.put(user,  note);
				donnees.put(img, notePersonne);
				flux.close();
			} else {
				Map notePersonne = new HashMap<String, Integer>();
				notePersonne.put(user, note);
				donnees.put(img, notePersonne);
			}
			

			try {
				ObjectOutputStream fluxOutput = new ObjectOutputStream(new FileOutputStream(fileNotes));

				fluxOutput.writeObject(donnees);
				fluxOutput.close();
				
			} catch (IOException ioe) {
				System.err.println(ioe);
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
	}
	
}
